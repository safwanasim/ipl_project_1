//5. Find the number of times each team won the toss and also won the match
function tossWonMatchWon(matchDataJSON) {
  let result = {}; //object for stroing the number of times each team won the toss and match
  matchDataJSON.filter((data) => {
    if (data.toss_winner == data.winner) {
      if (result[data.toss_winner]) {
        //checking if the toss winner is already in the result object
        result[data.toss_winner] += 1;
      } else {
        result[data.toss_winner] = 1;
      }
      return data;
    }
  });
  return result;
}
//end of function
//
//
//
//6. Find a player who has won the highest number of Player of the Match awards for each season
function playerOfMatch(matchDataJSON) {
  let players = matchDataJSON.reduce((accumulator, current) => {
    let season = current.season; //storing season of current element
    let playerOfMatch = current.player_of_match;
    if (accumulator[season]) {
      //checking if season already present
      if (accumulator[season][playerOfMatch]) {
        accumulator[season][playerOfMatch] += 1;
      } else {
        accumulator[season][playerOfMatch] = 1;
      }
    } else {
      accumulator[season] = {};
      //assigining an object to hold values season and the number
    }
    return accumulator;
  }, {});

  //converting object to K,V pairs
  playersArray = Object.entries(players);

  //reducing the array of k,v pairs to get top player of match
  playersArray = playersArray.reduce((accumulator, current) => {
    if (!accumulator[current[0]]) {
      let topPlayerOfMatch = Object.entries(current[1])
        .sort((a, b) => b[1] - a[1]) //sorting the object of k,v pairs
        .slice(0, 1); //getting the top element

      accumulator[current[0]] = convertNestedArraystoObject(topPlayerOfMatch);
    }
    return accumulator;
  }, {});
  return playersArray;
}
//end of function
//
//
//
//7. Find the highest number of times one player has been dismissed by another player
function bowlerSuperOvers(deliveriesDataJSON) {
  const result = deliveriesDataJSON
    .filter((data) => {
      if (data.is_super_over == 1) {
        return data;
      }
    }) //filtering out the data to only get super overs
    .reduce((acc, current) => {
      //using reduce to make an object holding runs given and number of deliveries
      let bowler = current.bowler;

      if (acc[bowler]) {
        acc[bowler].numberOfDeliveries += 1;
        acc[bowler].runsGiven += parseInt(current.total_runs);
      } else {
        acc[bowler] = {};
        acc[bowler].runsGiven = parseInt(current.total_runs);
        acc[bowler].numberOfDeliveries = 1;
      }
      return acc;
    }, {});

  //using map to calculate overs and economy based on the above result data structure
  let bowlersAndEconomy = Object.entries(result).map((item) => {
    //calculating overs
    item[1]["overs"] = Number(
      parseFloat(item[1]["numberOfDeliveries"] / 6).toFixed(2)
    );
    //calculating economy based on above overs
    item[1]["economy"] = Number(
      parseFloat(item[1]["runsGiven"] / item[1]["overs"]).toFixed(2)
    );
    return item;
  });

  bowlersAndEconomy = bowlersAndEconomy
    .sort((a, b) => {
      return a[1]["economy"] - b[1]["economy"];
      //sorting the data according to economy
    })
    .slice(0, 1); //getting the top most economical player

  bowlersAndEconomy = convertNestedArraystoObject(bowlersAndEconomy);
  //getting nested arrays to objects
  return bowlersAndEconomy;
}
//end of function
//
//
//
//8. Find the highest number of times one player has been dismissed by another player
function highestNumberOfTimesDismissed(deliveriesDataJSON) {
  let playersDismissed = deliveriesDataJSON
    .filter((data) => {
      if (data.player_dismissed) {
        return data;
      }
    })
    .reduce((acc, curr) => {
      let batsman = curr.batsman;
      let dismissedBy;
      //calculating dismissal player based on kind of dismissal
      if (
        curr.dismissal_kind == "run out" ||
        curr.dismissal_kind == "stumped"
      ) {
        dismissedBy = curr.fielder;
      } else {
        dismissedBy = curr.bowler;
      }
      if (acc[batsman]) {
        //seeing if there is a batsman in the accumulator object
        if (acc[batsman][dismissedBy]) {
          acc[batsman][dismissedBy] += 1;
        } else {
          acc[batsman][dismissedBy] = 1;
          //acc[batsman][dismissedBy].dismissedTimes = 1;
        }
      } else {
        acc[batsman] = {};
        acc[batsman][dismissedBy] = 1;
        //acc[batsman][dismissedBy].dismissedTimes = 1;
      }
      return acc;
    }, {});

  //converting objects to k,v pair arrays
  playersDismissed = Object.entries(playersDismissed);
  //reducing the above array to sort and get the top player dismissed
  playersDismissed = playersDismissed.reduce((acc, curr) => {
    if (!acc[curr[0]]) {
      let sortedObject = Object.entries(curr[1]).sort((a, b) => {
        return b[1] - a[1];
      });
      let topElement = sortedObject[0];
      //filtering the array and fetching the results based on the top element
      let topDismissers = sortedObject.filter((element) => {
        return topElement[1] == element[1];
      });

      acc[curr[0]] = convertNestedArraystoObject(topDismissers); //converting nested arrays to objects
    }
    return acc;
  }, {});
  return playersDismissed;
}
//end of function
//
//
//
//9. Batsman Strike Rate
function batsmanStrikeRate(matches, delivery) {
  let result = delivery.map((ball) => {
    let year = matches.find((match) => ball.match_id == match.id);
    ball["season"] = year.season;
    return ball;
  });
  result = result.reduce((acc, curr) => {
    if (!acc[curr.batsman]) {
      acc[curr.batsman] = {};
      acc[curr.batsman][curr.season] = [];
      acc[curr.batsman][curr.season][0] = 1;
      acc[curr.batsman][curr.season][1] = parseInt(curr.batsman_runs);
    } else {
      if (acc[curr.batsman][curr.season]) {
        acc[curr.batsman][curr.season][0] += 1;
        acc[curr.batsman][curr.season][1] += parseInt(curr.batsman_runs);
      } else {
        acc[curr.batsman][curr.season] = [];
        acc[curr.batsman][curr.season][0] = 1;
        acc[curr.batsman][curr.season][1] = parseInt(curr.batsman_runs);
      }
    }
    return acc;
  }, {});

  result = Object.entries(result).reduce((acc, curr) => {
    acc[curr[0]] = Object.entries(curr[1]).reduce((acc, curr) => {
      //dividing number  of deliveries by runs to get strike rate
      acc[curr[0]] = (100 * (curr[1][1] / curr[1][0])).toFixed(2);
      return acc;
    }, {});
    return acc;
  }, {});
  //console.log(result);
  return result;
}

//this function converts nested arrays to nested object
//this is used 4 times across this project
function convertNestedArraystoObject(array) {
  return array.reduce((acc, curr) => {
    //converting nested array to nested object
    if (!acc[curr[0]]) {
      acc[curr[0]] = curr[1];
    }
    return acc;
  }, {});
}

module.exports = {
  playerOfMatch,
  tossWonMatchWon,
  bowlerSuperOvers,
  batsmanStrikeRate,
  highestNumberOfTimesDismissed,
  convertNestedArraystoObject,
};
