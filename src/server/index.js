const fs = require("fs");
const CSVToJSON = require("csvtojson");
const matchesFile = "../data/matches.csv";
const deliveriesFile = "../data/deliveries.csv";

//importing functions from ipl file
const iplFunctions = require("./ipl.js"); //first 4 questions
const iplExtraFunctions = require("./iplExtras"); //extra 5 questions

//IIFE function passing two csv files
((matchesFile, deliveriesFile) => {
  //this is the main function that converts csv to json and passes it as arguments to other ipl functions

  CSVToJSON()
    .fromFile(matchesFile)
    .then((matchDataJSON) => {
      //getting the converted JSON data for matches

      CSVToJSON()
        .fromFile(deliveriesFile)
        .then((deliveriesDataJSON) => {
          //getting the converted deliveries JSON data from csv file
          //
          //
          /* Each of the below functions takes matches JSON or deliveries JSON and passes it to the functions from ./ipl.js, does all calculations and then saves the o/p in "../src/public" */

          //1. Number Of Matches played per year for all the years in IPL
          let matchesPlayedForAllYearsInIPL =
            iplFunctions.numberOfMatches(matchDataJSON);

          //passing name and content of file to function
          writeToOutputFile(
            "matchesPlayedForAllYearsInIPL",
            matchesPlayedForAllYearsInIPL
          );
          //--------------------------------------
          //
          //
          //2. Number Of Matches won per team per year in IPL
          let numberOfMatchesWonPerTeamPerYear =
            iplFunctions.numberOfMatchesWonPerTeam(matchDataJSON);

          //passing name and content of file to function
          writeToOutputFile(
            "numberOfMatchesWonPerTeamPerYear",
            numberOfMatchesWonPerTeamPerYear
          );
          //--------------------------------------
          //
          //
          //3.Extra runs conceded per team in the year 2016
          let extraRunsConcededPerTeamIn2016 = iplFunctions.extraRunsConceded(
            matchDataJSON,
            deliveriesDataJSON
          );

          //passing name and content of file to function
          writeToOutputFile(
            "extraRunsConcededPerTeamIn2016",
            extraRunsConcededPerTeamIn2016
          );
          //--------------------------------------
          //
          //
          //4. Top 10 economical bowlers of year 2015
          let top10economicalbowlers = iplFunctions.top10economicalbowlers(
            matchDataJSON,
            deliveriesDataJSON
          );

          //passing name and content of file to function
          writeToOutputFile("top10economicalbowlers", top10economicalbowlers);
          //--------------------------------------
          //
          //
          //5. Find the number of times each team won the toss and also won the match
          let matchWonAndTossWon =
            iplExtraFunctions.tossWonMatchWon(matchDataJSON);

          //passing name and content of file to function
          writeToOutputFile("matchWonAndTossWo", matchWonAndTossWon);
          //--------------------------------------
          //
          //
          //6. Find a player who has won the highest number of Player of the Match awards for each season
          let playerOfMatch = iplExtraFunctions.playerOfMatch(matchDataJSON);

          //passing name and content of file to function
          writeToOutputFile("playerOfMatch", playerOfMatch);
          //--------------------------------------
          //
          //
          //7. Bowler with best economy in superovers
          let bowlerWithBestEconomyInSuperOver =
            iplExtraFunctions.bowlerSuperOvers(deliveriesDataJSON);

          //passing name and content of file to function
          writeToOutputFile(
            "bowlerWithBestEconomyInSuperOver",
            bowlerWithBestEconomyInSuperOver
          );
          //--------------------------------------
          //
          //8.Find the highest number of times one player has been dismissed by another player
          let highestNumberOfTimesDismissed =
            iplExtraFunctions.highestNumberOfTimesDismissed(deliveriesDataJSON);

          //passing name and content of file to function
          writeToOutputFile(
            "highestNumberOfTimesDismissed",
            highestNumberOfTimesDismissed
          );
          //--------------------------------------
          //
          //
          //9.Find the strike rate of a batsman for each season
          let strikeRateOfBatsman = iplExtraFunctions.batsmanStrikeRate(
            matchDataJSON,
            deliveriesDataJSON
          );

          //passing name and content of file to function
          writeToOutputFile("strikeRateOfBatsman", strikeRateOfBatsman);
        });
    });
})(matchesFile, deliveriesFile);

//passing name and content of file to write
function writeToOutputFile(nameOfFile, contentOfFile) {
  fs.writeFileSync(
    `../public/output/${nameOfFile}.json`,
    JSON.stringify(contentOfFile),
    "utf-8",
    (error) => {
      console.log(`Failed writing to ${nameOfFile} ${error}`);
    }
  );
}

/*(async () => {  //this code also works perfectly fine
  try {
    matchDataJSON = await CSVToJSON().fromFile("../data/matches.csv");
    deliveriesDataJSON = await CSVToJSON().fromFile("../data/deliveries.csv");

    fs.writeFileSync("../data/match.json", JSON.stringify(matchDataJSON));

    fs.writeFileSync(
      "../data/deliveries.json",
      JSON.stringify(deliveriesDataJSON)
    );

    //1. Number Of Matches played per year for all the years in IPL
    let matchesPlayedForAllYearsInIPL =
      iplFunctions.numberOfMatches(matchDataJSON);
    fs.writeFileSync(
      "../public/output/matchesPlayedForAllYearsInIPL.json",
      JSON.stringify(matchesPlayedForAllYearsInIPL)
    );
    //and converted matches data
    //
    //2. Number Of Matches won per team per year in IPL
    let numberOfMatchesWonPerTeamPerYear =
      iplFunctions.numberOfMatchesWonPerTeam(matchDataJSON);
    fs.writeFileSync(
      "../public/output/numberOfMatchesWonPerTeamPerYear.json",
      JSON.stringify(numberOfMatchesWonPerTeamPerYear)
    );
    //
    //
    //3.Extra runs conceded per team in the year 2016
    let extraRunsConcededPerTeamIn2016 = iplFunctions.extraRunsConceded(
      matchDataJSON,
      deliveriesDataJSON
    );
    fs.writeFileSync(
      "../public/output/extraRunsConcededPerTeamIn2016.json",
      JSON.stringify(extraRunsConcededPerTeamIn2016)
    );
    //
    //
    //4. Top 10 economical bowlers of year 2015
    let top10economicalbowlers = iplFunctions.top10economicalbowlers(
      matchDataJSON,
      deliveriesDataJSON
    );
    fs.writeFileSync(
      "../public/output/top10economicalbowlers.json",
      JSON.stringify(top10economicalbowlers)
    );
    //
    //
    //5. Find the number of times each team won the toss and also won the match
    let matchWonAndTossWon = iplFunctions.tossWonMatchWon(matchDataJSON);
    fs.writeFileSync(
      "../public/output/teamsWinTossAndMatch.json",
      JSON.stringify(matchWonAndTossWon)
    );
    //
    //
    //6. Find a player who has won the highest number of Player of the Match awards for each season
    let playerOfMatch = iplFunctions.playerOfMatch(matchDataJSON);
    fs.writeFileSync(
      "../public/output/playerOfMatch.json",
      JSON.stringify(playerOfMatch)
    );
    //
    //
    //7. Bowler with best economy in superovers
    let bowlerWithBestEconomyInSuperOver =
      iplFunctions.bowlerSuperOvers(deliveriesDataJSON);
    fs.writeFileSync(
      "../public/output/bowlerWithBestEconomyInSuperOver.json",
      JSON.stringify(bowlerWithBestEconomyInSuperOver)
    );
    //
    //
    //8.Find the highest number of times one player has been dismissed by another player
    let highestNumberOfTimesDismissed =
      iplFunctions.highestNumberOfTimesDismissed(deliveriesDataJSON);
    fs.writeFileSync(
      "../public/output/highestNumberOfTimesDismissed.json",
      JSON.stringify(highestNumberOfTimesDismissed)
    );
    //
    //
    //
    //9.Find the strike rate of a batsman for each season
    let strikeRateOfBatsman = iplFunctions.batsmanStrikeRate(
      matchDataJSON,
      deliveriesDataJSON
    );
    fs.writeFileSync(
      "../public/output/strikeRateOfBatsman.json",
      JSON.stringify(strikeRateOfBatsman)
    );
  } catch (err) {
    console.log(err);
  }
})();
*/
