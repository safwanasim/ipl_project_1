//1. Number Of Matches played per year for all the years in IPL

const { convertNestedArraystoObject } = require("./iplExtras");

/* The below function takes in matchData(JSON) as argument, uses reduce function to reduce the objects and then returns the results*/
function numberOfMatches(matchData) {
  let numOfMatches = matchData.reduce((accumulator, current) => {
    if (accumulator[current.season]) {
      accumulator[current.season] += 1;
    } else {
      accumulator[current.season] = 1;
    }
    return accumulator;
  }, {});
  return numOfMatches;
}
//
//
//
//2. Number Of Matches won per team per year in IPL
function numberOfMatchesWonPerTeam(matchData) {
  let numberOfMatchesWonPerTeam = matchData.reduce((accumulator, current) => {
    if (accumulator[current.season]) {
      //checking if current season is present in accumulator object (the object to be returned)
      //if its there, increment it, if not initialize it to 1
      if (accumulator[current.season][current.winner]) {
        accumulator[current.season][current.winner] += 1;
      } else {
        accumulator[current.season][current.winner] = 1;
      }
    } else {
      accumulator[current.season] = {};
    }
    return accumulator;
  }, {});
  return numberOfMatchesWonPerTeam;
}
//
//
//
//3. Extra runs conceded per team in the year 2016
function extraRunsConceded(matchDataJSON, deliveriesData) {
  //getting the Ids of matches in 2016 from the getIdsOfMatches function
  const idsOfMatchesIn2016 = getIDsOfMatches(matchDataJSON, 2016);

  //matching above ids with deliveries data and getting the extra_runs per bowling team
  let result = {}; //for storing the object to be returned
  let bowling_team; //storing data["bowling_team"] --> value changes every iteration of filter
  let extra_runs; //variable to store the extra_runs --> value changes every iteration of filter

  /*looping through the IDs and using Array.filter() on every ID to get the match from deliveries data and then extracting bowling team and extra_runs*/
  for (let index = 0; index < idsOfMatchesIn2016.length; index++) {
    deliveriesData.filter((data) => {
      //filtering the data based on the Id
      if (data.match_id == idsOfMatchesIn2016[index]) {
        bowling_team = data["bowling_team"];
        extra_runs = data["extra_runs"];
        if (result[bowling_team]) {
          //checking if the current element is present in result object
          result[bowling_team] += parseInt(extra_runs);
        } else {
          result[bowling_team] = parseInt(extra_runs);
        }
      }
    });
  }
  return result;
}
//
//
//
//4.Top 10 economical bowlers in the year 2015
function top10economicalbowlers(matchDataJSON, deliveriesDataJSON) {
  //calling the getIDsOfMatches() to get match Id of matches in 2015
  const idsOfMatchesIn2015 = getIDsOfMatches(matchDataJSON, 2015);

  let bowlerObject = {}; //initializing the returning object
  let bowler; //variable to store the current data's bowler
  let runs; //variable to store the current data's runs

  //extracting bowlers of 2015 and their total runs for the season of 2015
  for (let index = 0; index < idsOfMatchesIn2015.length; index++) {
    deliveriesDataJSON.filter((data) => {
      if (data.match_id == idsOfMatchesIn2015[index]) {
        bowler = data.bowler;
        runs = data.total_runs;
        if (bowlerObject[bowler]) {
          bowlerObject[bowler].numberOfDeliveries += 1;
          bowlerObject[bowler].runs += parseInt(runs);
        } else {
          bowlerObject[bowler] = {};
          bowlerObject[bowler].numberOfDeliveries = 1;
          bowlerObject[bowler].runs = parseInt(runs);
        }
      }
    });
  }

  //calculating number of overs and then calculating economy of bowlers based on the above data
  let bowlersAndEconomy = Object.entries(bowlerObject).map((item) => {
    item[1]["overs"] = Number(
      parseFloat(item[1]["numberOfDeliveries"] / 6).toFixed(2)
    );
    item[1]["economy"] = Number(
      parseFloat(item[1]["runs"] / item[1]["overs"]).toFixed(2)
    );
    return item;
  });

  //sorting the above array and slicing it to get the top 10 bowlers
  let top10economical = bowlersAndEconomy
    .sort(function (a, b) {
      return a[1]["economy"] - b[1]["economy"];
    })
    .slice(0, 10); //gets top 10 from the entire array of bowlers
  top10economical = convertNestedArraystoObject(top10economical);

  return top10economical;
}
//top10economical function ends here

//
//
//
//this function takes in two arguments - JSON and year. It then returns the IDs of matches for the given year
function getIDsOfMatches(matchDataJSON, year) {
  let idsOfMatches = [];
  matchDataJSON.filter((data) => {
    //getting IDs of matches for given year
    if (data.season == year) {
      idsOfMatches.push(data.id);
    }
  });
  return idsOfMatches;
}
//
//
//
//exporting all functions
module.exports = {
  top10economicalbowlers,
  extraRunsConceded,
  numberOfMatches,
  numberOfMatchesWonPerTeam,
};
